﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Controls;

namespace Sipol.Net.Extensions.Wpf.Unit.Tests
{
    [TestFixture()]
    [Apartment(ApartmentState.STA)]
    public class TreeViewExt_Tests
    {
        #region IterateThroughAllItems Tests

        [Test()]
        public void IterateThroughAllItems__EmptyTree__DontIterate()
        {
            // Arrange
            TreeView treeView = new TreeView();


            int count = 0;
            // Act
            treeView.IterateThroughAllItems(item =>
            {
                count++;
            });

            // Assert
            Assert.That(count,
                        Is.EqualTo(0)
                        );

        }

        [Test()]
        public void IterateThroughAllItems__OlnyOneItem__IterateOnOneNode()
        {
            // Arrange
            TreeView treeView = new TreeView();
            TreeViewItem someItem = new TreeViewItem() { Header = "test item" };
            treeView.Items.Add(someItem);


            TreeViewItem lastItem = null;
            int count = 0;
            // Act
            treeView.IterateThroughAllItems(item =>
            {
                if (item is TreeViewItem)
                    lastItem = item as TreeViewItem;

                count++;
            });

            // Assert
            Assert.That(count,
                        Is.EqualTo(1)
                        );

            Assert.That(lastItem,
                        Is.SameAs(someItem)
                        );

        }


        [Test()]
        public void IterateThroughAllItems__FullTree__IterateAllNodeExcalyOneInEachNode()
        {
            // Arrange
            TreeView treeView = new TreeView();
            List<TreeViewItem> lstItems = new List<TreeViewItem>();
            Dictionary<TreeViewItem, int> items = new Dictionary<TreeViewItem, int>();

            for(int i=0;i<10;i++)
                lstItems.Add(new TreeViewItem() { Header = "Item " + i.ToString() });

            foreach (TreeViewItem item in lstItems)
                items.Add(item, 0);

            lstItems[0].Items.Add(lstItems[2]);
            lstItems[2].Items.Add(lstItems[3]);
            lstItems[2].Items.Add(lstItems[4]);
            lstItems[3].Items.Add(lstItems[5]);

            lstItems[1].Items.Add(lstItems[6]);
            lstItems[1].Items.Add(lstItems[7]);
            lstItems[7].Items.Add(lstItems[8]);
            lstItems[8].Items.Add(lstItems[9]);

            treeView.Items.Add(lstItems[0]);
            treeView.Items.Add(lstItems[1]);

            // Act
            treeView.IterateThroughAllItems(item =>
            {
                if (items.ContainsKey(item as TreeViewItem))
                    items[item as TreeViewItem]++;
                else throw new Exception("not item in list");
            });

            // Assert
            foreach (TreeViewItem item in lstItems)
                Assert.That(items[item],
                            Is.EqualTo(1),
                            "Wrong item iterate [" + item.Header + "]: " + items[item].ToString()
                            );
        }


        #endregion


        #region GetParentOfItem Tests


        [Test()]
        public void GetParentOfItem__byDefault__ParentOfItem()
        {
            // Arrange
            TreeView treeView = new TreeView();
            List<TreeViewItem> lstItems = new List<TreeViewItem>();

            for (int i = 0; i < 10; i++)
                lstItems.Add(new TreeViewItem() { Header = "/[<Item " + i.ToString() + ">]/" });


            lstItems[0].Items.Add(lstItems[2]);
            lstItems[2].Items.Add(lstItems[3]);
            lstItems[2].Items.Add(lstItems[4]);
            lstItems[3].Items.Add(lstItems[5]);
            

            lstItems[1].Items.Add(lstItems[6]);
            lstItems[1].Items.Add(lstItems[7]);
            lstItems[7].Items.Add(lstItems[8]);
            lstItems[8].Items.Add(lstItems[9]);

            treeView.Items.Add(lstItems[0]);
            treeView.Items.Add(lstItems[1]);

            // Act

            TreeViewItem parent = TreeViewExt.GetParentOfItem(lstItems[5]);



            // Assert
            Assert.That(parent,
                        Is.Not.Null &
                        Is.SameAs(lstItems[3])
                        );
        }


        [Test()]
        public void GetParentOfItem__ParentItemHasTextBox__ParentOfItem()
        {
            // Arrange
            TreeView treeView = new TreeView();
            List<Control> lstItems = new List<Control>();

            for (int i = 0; i < 10; i++)
                lstItems.Add(new TreeViewItem() { Header = "/[<Item " + i.ToString() + ">]/" });

            lstItems.Add(new TextBox() { Text = "some text" });


            (lstItems[0] as TreeViewItem).Items.Add(lstItems[2]);
            (lstItems[2] as TreeViewItem).Items.Add(lstItems[3]);
            (lstItems[2] as TreeViewItem).Items.Add(lstItems[4]);
            (lstItems[3] as TreeViewItem).Items.Add(lstItems[10]);
            (lstItems[3] as TreeViewItem).Items.Add(lstItems[5]);


            (lstItems[1] as TreeViewItem).Items.Add(lstItems[6]);
            (lstItems[1] as TreeViewItem).Items.Add(lstItems[7]);
            (lstItems[7] as TreeViewItem).Items.Add(lstItems[8]);
            (lstItems[8] as TreeViewItem).Items.Add(lstItems[9]);

            treeView.Items.Add(lstItems[0]);
            treeView.Items.Add(lstItems[1]);

            // Act

            TreeViewItem parent = TreeViewExt.GetParentOfItem(lstItems[5] as TreeViewItem);



            // Assert
            Assert.That(parent,
                        Is.Not.Null &
                        Is.SameAs(lstItems[3])
                        );
        }

        [Test()]
        public void GetParentOfItem__ParentItemIsOtherItemsControl__ParentIsNearestTreeViewItem()
        {
            // Arrange
            TreeView treeView = new TreeView();
            List<Control> lstItems = new List<Control>();

            for (int i = 0; i < 10; i++)
                lstItems.Add(new TreeViewItem() { Header = "/[<Item " + i.ToString() + ">]/" });

            lstItems.Add(new Menu());


            (lstItems[0] as TreeViewItem).Items.Add(lstItems[2]);
            (lstItems[2] as TreeViewItem).Items.Add(lstItems[3]);
            (lstItems[2] as TreeViewItem).Items.Add(lstItems[4]);
            (lstItems[3] as TreeViewItem).Items.Add(lstItems[10]);
            (lstItems[10] as Menu).Items.Add(lstItems[5]);


            (lstItems[1] as TreeViewItem).Items.Add(lstItems[6]);
            (lstItems[1] as TreeViewItem).Items.Add(lstItems[7]);
            (lstItems[7] as TreeViewItem).Items.Add(lstItems[8]);
            (lstItems[8] as TreeViewItem).Items.Add(lstItems[9]);

            treeView.Items.Add(lstItems[0]);
            treeView.Items.Add(lstItems[1]);

            // Act
            TreeViewItem parent = TreeViewExt.GetParentOfItem(lstItems[5] as TreeViewItem);

            // Assert
            Assert.That(parent,
                        Is.Not.Null &
                        Is.SameAs(lstItems[3])
                        );
        }


        [Test()]
        public void GetParentOfItem__OnRootItem__ParentIsNull()
        {
            // Arrange
            TreeView treeView = new TreeView();
            List<Control> lstItems = new List<Control>();

            for (int i = 0; i < 10; i++)
                lstItems.Add(new TreeViewItem() { Header = "/[<Item " + i.ToString() + ">]/" });

            lstItems.Add(new Menu());


            (lstItems[0] as TreeViewItem).Items.Add(lstItems[2]);
            (lstItems[2] as TreeViewItem).Items.Add(lstItems[3]);
            (lstItems[2] as TreeViewItem).Items.Add(lstItems[4]);
            (lstItems[3] as TreeViewItem).Items.Add(lstItems[10]);
            (lstItems[10] as Menu).Items.Add(lstItems[5]);


            (lstItems[1] as TreeViewItem).Items.Add(lstItems[6]);
            (lstItems[1] as TreeViewItem).Items.Add(lstItems[7]);
            (lstItems[7] as TreeViewItem).Items.Add(lstItems[8]);
            (lstItems[8] as TreeViewItem).Items.Add(lstItems[9]);

            treeView.Items.Add(lstItems[0]);
            treeView.Items.Add(lstItems[1]);

            // Act

            TreeViewItem parent = TreeViewExt.GetParentOfItem(lstItems[0] as TreeViewItem);



            // Assert
            Assert.That(parent,
                        Is.Null 
                        );
        }

        #endregion


        #region GetParentsOfItem Tests


        [Test()]
        public void GetParentsOfItem__byDefault__ArrayOfParentsTreeViewItems()
        {
            // Arrange
            TreeView treeView = new TreeView();
            List<Control> lstItems = new List<Control>();

            for (int i = 0; i < 10; i++)
                lstItems.Add(new TreeViewItem() { Header = "/[<Item " + i.ToString() + ">]/" });

            lstItems.Add(new Menu());


            (lstItems[0] as TreeViewItem).Items.Add(lstItems[2]);
            (lstItems[2] as TreeViewItem).Items.Add(lstItems[3]);
            (lstItems[2] as TreeViewItem).Items.Add(lstItems[4]);
            (lstItems[3] as TreeViewItem).Items.Add(lstItems[10]);
            (lstItems[10] as Menu).Items.Add(lstItems[5]);


            (lstItems[1] as TreeViewItem).Items.Add(lstItems[6]);
            (lstItems[1] as TreeViewItem).Items.Add(lstItems[7]);
            (lstItems[7] as TreeViewItem).Items.Add(lstItems[8]);
            (lstItems[8] as TreeViewItem).Items.Add(lstItems[9]);

            treeView.Items.Add(lstItems[0]);
            treeView.Items.Add(lstItems[1]);

            // Act

            TreeViewItem[] parents = TreeViewExt.GetParentsOfItem(lstItems[5] as TreeViewItem);


            // Assert
            Assert.That(parents,
                        Is.Not.Null &
                        Is.EquivalentTo(new TreeViewItem[] { lstItems[3] as TreeViewItem, lstItems[2] as TreeViewItem, lstItems[0]  as TreeViewItem })
                    );
        }


        [Test()]
        public void GetParentsOfItem__OnRootElement__EmpatyArray()
        {
            // Arrange
            TreeView treeView = new TreeView();
            List<Control> lstItems = new List<Control>();

            for (int i = 0; i < 10; i++)
                lstItems.Add(new TreeViewItem() { Header = "/[<Item " + i.ToString() + ">]/" });

            lstItems.Add(new Menu());

            (lstItems[0] as TreeViewItem).Items.Add(lstItems[2]);
            (lstItems[2] as TreeViewItem).Items.Add(lstItems[3]);
            (lstItems[2] as TreeViewItem).Items.Add(lstItems[4]);
            (lstItems[3] as TreeViewItem).Items.Add(lstItems[10]);
            (lstItems[10] as Menu).Items.Add(lstItems[5]);


            (lstItems[1] as TreeViewItem).Items.Add(lstItems[6]);
            (lstItems[1] as TreeViewItem).Items.Add(lstItems[7]);
            (lstItems[7] as TreeViewItem).Items.Add(lstItems[8]);
            (lstItems[8] as TreeViewItem).Items.Add(lstItems[9]);

            treeView.Items.Add(lstItems[0]);
            treeView.Items.Add(lstItems[1]);

            // Act

            TreeViewItem[] parents = TreeViewExt.GetParentsOfItem(lstItems[1] as TreeViewItem);


            // Assert
            Assert.That(parents,
                        Is.Not.Null &
                        Is.Empty
                    );
        }

        #endregion

    }
}