﻿using NUnit.Framework;

using System.Threading;
using System.Windows;
using System.Windows.Controls;

namespace Sipol.Net.Extensions.Wpf.Unit.Tests
{
    [TestFixture()]
    [Apartment(ApartmentState.STA)]
    public class GridExt_Tests
    {
        #region GetRowColumn Tests


        //[Test()]
        public void GetRowColumn__CheckColumn__CorrectColumn()
        {
            // Arrange
            StackPanel pnl = new StackPanel();
            pnl.Height = 300;
            pnl.Width = 500;
            Grid grid = new Grid();
            grid.Height = 150d;
            grid.MinHeight = 150d;
            grid.VerticalAlignment = VerticalAlignment.Stretch;
            grid.HorizontalAlignment = HorizontalAlignment.Stretch;
            
            grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(100) });
            grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(150) });
            grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(250) });


            for (int i = 0; i < 10; i++)
            {
                RowDefinition rowDef = new RowDefinition() { Height = new GridLength(30d), MinHeight = 30d, MaxHeight = 30d, IsEnabled = true };
                grid.RowDefinitions.Add(rowDef);
                int rowIndex = grid.RowDefinitions.IndexOf(rowDef);
                Label lbl = new Label() { Height = 30d, Content = "some text", VerticalAlignment = VerticalAlignment.Stretch, VerticalContentAlignment = VerticalAlignment.Stretch };
                grid.Children.Add(lbl);
                Grid.SetColumn(lbl, 0);
                Grid.SetRow(lbl, rowIndex);
                lbl.Refresh();
                
            }

            grid.Refresh();
            pnl.Children.Add(grid);
            // Act
            int row = -1;
            int column = -1;
            grid.GetRowColumn(new Point(251, 32), out row, out column);

            // Assert
            Assert.That(column,
                        Is.EqualTo(2)
                        );


        }


        #endregion
    }
}