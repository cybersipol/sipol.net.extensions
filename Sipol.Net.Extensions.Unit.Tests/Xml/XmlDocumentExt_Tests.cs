﻿using NUnit.Framework;
using System.Threading;
using System.Xml;

namespace Sipol.Net.Extensions.Xml.Unit.Tests
{
    [TestFixture()]
    
    public class XmlDocumentExt_Tests
    {
        #region GetNodeLevel Tests

        [Test()]
        public void GetNodeLevel_OlnyRootElement_Level1()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml("<root/>");

            Assert.That(doc.GetNodeLevel(doc.DocumentElement),
                            Is.EqualTo(1)
                );

        }


        [Test()]
        public void GetNodeLevel_ChildElement_Level2()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml("<root><child/></root>");

            Assert.That(
                    doc.GetNodeLevel(doc.DocumentElement.ChildNodes[0]),
                    Is.EqualTo(2)
                );

        }

        [Test()]
        public void GetNodeLevel_ChildElementWithText_TextLevel3()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml("<root><child>test</child></root>");

            Assert.That(
                    doc.GetNodeLevel(doc.DocumentElement.ChildNodes[0].ChildNodes[0]),
                    Is.EqualTo(3)
                );

        }

        [Test()]
        public void GetNodeLevel_SubChildElement_Level3()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml("<root><child/><child><child2/></child></root>");

            Assert.That(
                    doc.GetNodeLevel(doc.DocumentElement.ChildNodes[1].ChildNodes[0]),
                    Is.EqualTo(3)
                );

        }
        #endregion



    }
}