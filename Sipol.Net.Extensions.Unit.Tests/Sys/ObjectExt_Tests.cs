﻿using NUnit.Framework;
using Sipol.Net.Extensions.Sys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sipol.Net.Extensions.Sys.Unit.Tests
{
    [TestFixture()]
    public class ObjectExt_Tests
    {

        #region IsObjectOfType Tests


        [Test()]
        public void IsObjectOfType__CompareStringTypeWithIntType__False()
        {
            // Arrange
            // Act
            bool result = ObjectExt.IsObjectOfType(String.Empty, typeof(int));

            // Assert
            Assert.That(result,
                         Is.False
            );
        }


        [Test()]
        public void IsObjectOfType__CompareNullTypeWithIntType__False()
        {
            // Arrange
            // Act
            bool result = ObjectExt.IsObjectOfType(null, typeof(int));

            // Assert
            Assert.That(result,
                         Is.False
            );
        }


        [Test()]
        public void IsObjectOfType__CompareStringTypeWithNull__False()
        {
            // Arrange
            // Act
            bool result = ObjectExt.IsObjectOfType(String.Empty, null);

            // Assert
            Assert.That(result,
                         Is.False
            );
        }


        [Test()]
        public void IsObjectOfType__CompareStringObjectWithStringType__True()
        {
            // Arrange
            // Act
            bool result = ObjectExt.IsObjectOfType(String.Empty, typeof(string));

            // Assert
            Assert.That(result,
                         Is.True
            );
        }

        [Test()]
        public void IsObjectOfType__CompareClassObjectWithoutGeneralizationWithStringType__False()
        {
            // Arrange
            // Act
            bool result = ObjectExt.IsObjectOfType(new SomeClassA(), typeof(string));

            // Assert
            Assert.That(result,
                         Is.False
            );
        }


        [Test()]
        public void IsObjectOfType__CompareClassObjectWithoutGeneralizationWithSameType__True()
        {
            // Arrange
            // Act
            bool result = ObjectExt.IsObjectOfType(new SomeClassA(), typeof(SomeClassA));

            // Assert
            Assert.That(result,
                         Is.True
            );
        }


        [Test()]
        public void IsObjectOfType__CompareClassObjectWithHisSubType__False()
        {
            // Arrange
            // Act
            bool result = ObjectExt.IsObjectOfType(new SomeClassA(), typeof(SomeClassB));

            // Assert
            Assert.That(result,
                         Is.False
            );
        }


        [Test()]
        public void IsObjectOfType__CompareClassObjectWithHisParentType__True()
        {
            // Arrange
            // Act
            bool result = ObjectExt.IsObjectOfType(new SomeClassB(), typeof(SomeClassA));

            // Assert
            Assert.That(result,
                         Is.True
            );
        }


        [Test()]
        public void IsObjectOfType__CompareClassObjectWithHisInterfaceType__True()
        {
            // Arrange
            // Act
            bool result = ObjectExt.IsObjectOfType(new SomeClassB(), typeof(ISomeInterface));

            // Assert
            Assert.That(result,
                         Is.True
            );
        }

        [Test()]
        public void IsObjectOfType__CompareClassObjectWithoutInterfaceWithInterface__False()
        {
            // Arrange
            // Act
            bool result = ObjectExt.IsObjectOfType(new SomeClassA(), typeof(ISomeInterface));

            // Assert
            Assert.That(result,
                         Is.False
            );
        }


        #endregion


        #region Help classes & interfaces

        private interface ISomeInterface
        {
            void method();
        }

        private class SomeClassA
        {

        }

        private class SomeClassB : SomeClassA, ISomeInterface
        {
            public void method()
            {
            }
        }




        #endregion

    }
}