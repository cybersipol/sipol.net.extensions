﻿using NUnit.Framework;

using System;

namespace Sipol.Net.Extensions.Sys.Unit.Tests
{
    [TestFixture()]
    public class StringExt_Tests
    {

        #region ToCurrency Tests

        [TestCase(".781.", 781.00d)]
        [TestCase("456.789.", 456789.00d)]
        [TestCase("123,123,123", 123123123d)]
        [TestCase("123.456.789", 123456789d)]
        [TestCase("123 456 789", 123456789d)]
        [TestCase("456.789", 456.789d)]
        [TestCase("12.01", 12.01d)]
        [TestCase("-12.01", -12.01d)]
        [TestCase("1.01300111222222", 1.01300111222222)]
        [TestCase("-0.01409911", -0.01409911d)]
        [TestCase("999,234,997,801.78", 999234997801.78d)]
        [TestCase("999.234.997.801.78", 999234997801.78d)]
        [TestCase("999,234 997801.781", 999234997801.781d)]
        [TestCase("999,234.997.801.781", 999234997801781d)]
        [TestCase("999.234.997.801,781", 999234997801.781d)]
        [TestCase(",781", 0.781d)]
        [TestCase(".781", 0.781d)]
        [TestCase("-,781", -0.781d)]
        [TestCase("-.781", -0.781d)]
        [TestCase("-.781.", -781.00d)]
        [TestCase("-781.", -781.00d)]
        [TestCase("-,781,", -781.00d)]
        [TestCase("-781,", -781.00d)]
        [TestCase("23", 23d)]
        [TestCase("-45", -45d)]
        [TestCase(",45,", 45d)]
        public void ToCurrency__CorrectCurrencyFormat__DecimalExpected(string input, Decimal expected)
        {
            // Arrange

            // Act
            Decimal result = input.ToCurrency();

            // Assert
            Assert.That(result,
                        Is.EqualTo(expected)
                        );
        }

        [TestCase("no currecny format")]
        [TestCase("...")]
        [TestCase(",,")]
        [TestCase("")]
        [TestCase("      ")]
        [TestCase("-------.--")]
        public void ToCurrency__IncorrectCurrencyFormat__ThrowFormatException( string input)
        {
            // Arrange
            // Act
            // Assert
            Assert.That(() =>
            {
                Decimal result = input.ToCurrency();
            }
            ,
            Throws.InstanceOf<FormatException>()
            );
            

        }

        #endregion

        #region CountChar Tests


        [TestCase('.',"how many dot are see...", 3)]
        [TestCase('.', ". . .", 3)]
        [TestCase(',', ".,.,.", 2)]
        [TestCase(',', ",,", 2)]
        [TestCase(' ', "how many space are see ?", 5)]
        [TestCase('.', "aaa aaa", 0)]
        [TestCase('.', "aaa.aaa", 1)]
        public void CountChar__byDefault__ReturnCorrectCountChar(char _ch, string in_string, int expectedCount )
        {
            // Arrange
            int result = 0;
            // Act
            result = in_string.CountChar(_ch);

            // Assert
            Assert.That(result,
                        Is.EqualTo(expectedCount)
                        );
        }


        #endregion
    }
}