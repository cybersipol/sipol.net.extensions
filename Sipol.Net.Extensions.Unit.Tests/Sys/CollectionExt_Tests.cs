﻿using NUnit.Framework;

using System.Collections;
using System.Collections.Generic;

namespace Sipol.Net.Extensions.Sys.Unit.Tests
{
    [TestFixture()]
    public class CollectionExt_Tests
    {
        #region IsEmpty Tests

        [Test()]
        public void IsEmpty__WhenCollectionIsNull__True()
        {
            // Arrange
            ICollection collection = null;            

            // Act
            bool result = CollectionExt.IsEmpty(collection);

            // Assert
            Assert.That(result,
                         Is.True
            );
        }


        [Test()]
        public void IsEmpty__WhenCollectionIsNotNullAndNoHasElements__True()
        {
            // Arrange
            ICollection collection = new List<string>();

            // Act
            bool result = CollectionExt.IsEmpty(collection);

            // Assert
            Assert.That(result,
                         Is.True
            );
        }


        [Test()]
        public void IsEmpty__WhenCollectionIsNotNullAndHasElements__False()
        {
            // Arrange
            ICollection collection = new List<string>()
            {
                "string1","string2"
            };

            // Act
            bool result = CollectionExt.IsEmpty(collection);

            // Assert
            Assert.That(result,
                         Is.False
            );
        }

        [Test()]
        public void IsEmpty__WhenArrayIsNull__True()
        {
            // Arrange
            string[] ii = null;
            // Act
            bool result = CollectionExt.IsEmpty(ii);

            // Assert
            Assert.That(result,
                         Is.True
            );
        }

        [Test()]
        public void IsEmpty__WhenObjectIsEmptyList__True()
        {
            // Arrange
            List<double> ii = new List<double>();
            // Act
            bool result = CollectionExt.IsEmpty((object)ii);

            // Assert
            Assert.That(result,
                         Is.True
            );
        }


        [Test()]
        public void IsEmpty__WhenObjectIsNotEmptyList__False()
        {
            // Arrange
            List<double> ii = new List<double>();
            ii.Add(1.3);
            // Act
            bool result = CollectionExt.IsEmpty((object)ii);

            // Assert
            Assert.That(result,
                         Is.False
            );
        }

        [Test()]
        public void IsEmpty__WhenObjectIsNullList__True()
        {
            // Arrange
            List<double> ii = null;
          
            // Act
            bool result = CollectionExt.IsEmpty((object)ii);

            // Assert
            Assert.That(result,
                         Is.True
            );
        }


        [Test()]
        public void IsEmpty__WhenObjectIsNullArray__True()
        {
            // Arrange
            double[] ii = null;

            // Act
            bool result = CollectionExt.IsEmpty((object)ii);

            // Assert
            Assert.That(result,
                         Is.True
            );
        }

        [Test()]
        public void IsEmpty__WhenObjectIsNotEmptyArray__False()
        {
            // Arrange
            double[] ii = new double[] { 1.3, 1.4 };
            
            // Act
            bool result = CollectionExt.IsEmpty((object)ii);

            // Assert
            Assert.That(result,
                         Is.False
            );
        }

        [Test()]
        public void IsEmpty__WhenArrayIsNotNullAndNoHasElements__True()
        {
            // Arrange
            string[] ii = new string[] { };
            // Act
            bool result = CollectionExt.IsEmpty(ii);

            // Assert
            Assert.That(result,
                         Is.True
            );
        }


        [Test()]
        public void IsEmpty__WhenArrayIsNotNullAndHasElements__False()
        {
            // Arrange
            int[] ii = new int[] { 1, 2, 3 };
            // Act
            bool result = CollectionExt.IsEmpty(ii);

            // Assert
            Assert.That(result,
                         Is.False
            );
        }

        #endregion

    }
}