﻿using System;

namespace Sipol.Net.Extensions.Sys
{
    public static class ObjectExt
    {
        public static bool IsObjectOfType(object obj, Type type)
        {
            if (obj is null)
                return false;

            if (type is null)
                return false;

            return type.IsInstanceOfType(obj);
        }

        public static bool IsObjectOfType<T>( object obj)
        {
            return IsObjectOfType(obj, typeof(T));
        }

    }
}
