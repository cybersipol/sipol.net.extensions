﻿using System;
using System.Collections;

namespace Sipol.Net.Extensions.Sys
{
    public static class CollectionExt
    {
        public static bool IsEmpty( ICollection collection )
        {
            return (collection?.Count ?? 0)<=0;
        }

        public static bool IsEmpty(Array array)
        {
            return (array?.Length ?? 0) <= 0;
        }

        public static bool IsEmpty( IList list )
        {
            return (list?.Count ?? 0) <= 0;
        }

        public static bool IsEmpty( object o)
        {
            if (o is null)
                return true;

            int? count = (int?) o.GetType()?.GetProperty("Count")?.GetValue(o);
            if (count is null)
                count = (int?)o.GetType()?.GetProperty("Length")?.GetValue(o);

            return (count != null && count > 0) ? false : true;
        }

    }
}
