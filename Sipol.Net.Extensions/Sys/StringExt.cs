﻿using System;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace Sipol.Net.Extensions.Sys
{
    public static class StringExt
    {
        public static string MD5( this string s, Encoding encoding )
        {
            if (encoding == null)
                return null;

            using (var provider = System.Security.Cryptography.MD5.Create())
            {
                StringBuilder builder = new StringBuilder();

                foreach (byte b in provider.ComputeHash(encoding.GetBytes(s)))
                    builder.Append(b.ToString("x2").ToLower());

                return builder.ToString();
            }
        }

        public static string MD5( this string s )
        {
            return MD5(s, Encoding.UTF8);
        }


        public static Decimal ToCurrency( this string @this )
        {
            string val = (@this ?? String.Empty).Trim();
            val = Regex.Replace(val, "([^0-9\\,\\.\\-]+)", "").Trim();
            //val = val.TrimEnd(new char[] { '.', ',' }).Trim();
            if (Regex.Replace(val, "([^0-9]+)", "").Length <= 0)
                throw new FormatException("No digits in text");

            bool minus = Regex.IsMatch(val, "^-");
            val = Regex.Replace(val, "([^0-9\\,\\.]+)", "").Trim();

            int dotIndex = val.LastIndexOf('.');
            int comIndex = val.LastIndexOf(',');

            char decimalChar = '.';
            char tousandChar = ',';

            int decIndex = val.Length;
            int decCount = 0;

            if (dotIndex > comIndex && dotIndex >= 0)
            {
                // dot is decimal point
                decimalChar = '.';
                tousandChar = ',';
                decIndex = dotIndex;
            }

            if (dotIndex < comIndex && comIndex >= 0)
            {
                // comma is decimal point
                decimalChar = ',';
                tousandChar = '.';
                decIndex = comIndex;
            }

            decCount = val.CountChar(decimalChar);


            if (decCount > 1 && Regex.IsMatch(val, "\\"+decimalChar+"([0-9]{3})$"))
            {
                int count = 0;
                int lastIndex = decIndex;
                do
                {
                    int startIndex = Math.Max(lastIndex - 3, 0);
                    int endIndex = lastIndex + 3;
                    endIndex = Math.Min(endIndex, val.Length - 1);
                    int len = endIndex - startIndex + 1;

                    string group = val.Substring(startIndex, len);

                    if (Regex.IsMatch(group, "([0-9]{1,3})\\"+decimalChar+"([0-9]{3})"))
                        count++;


                    lastIndex = val.LastIndexOf(decimalChar, lastIndex - 1);
                }
                while (lastIndex >= 0);

                if (count >= decCount)
                    decIndex = val.Length;

            }


            int secondPartLength = val.Length - decIndex - 1;

            string[] parts = new string[] 
            {
              val.Substring(0, decIndex),
              (secondPartLength<=0 ? String.Empty : val.Substring(decIndex + 1, secondPartLength))
            };

            for (int i=0;i<parts.Length;i++)
                parts[i] = parts[i].Replace("" + decimalChar, "").Replace("" + tousandChar, "");

            val = String.Join(".", parts);

            if (Regex.IsMatch(val, "\\" + decimalChar + "$"))
            {
                val = val.Replace(".", "");
                val = val + ".00";
            }

            NumberFormatInfo nfi = new NumberFormatInfo();
            nfi.NumberDecimalSeparator = ".";
            nfi.NumberDecimalDigits = 2;
            nfi.NegativeSign = "-";
            nfi.NumberGroupSeparator = "";
            nfi.NumberGroupSizes = new int[] { 0 };

            Decimal dec = Decimal.Parse(val, nfi);
            if (minus)
                dec = Decimal.Negate(dec);

            return dec;
        }

        public static int CountChar(this string @this, char _char)
        {
            int firstIndex = @this.IndexOf(_char);
            int lastIndex = @this.LastIndexOf(_char);

            if (firstIndex < 0 || lastIndex < 0)
                return 0;

            if (firstIndex == lastIndex)
                return 1;

            int count = 2;
            for (int i = firstIndex + 1; i < lastIndex; i++)
                if (@this[i] == _char)
                    count++;

            return count;
        }

    }
}