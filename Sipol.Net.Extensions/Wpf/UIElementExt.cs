﻿using System;
using System.Windows;
using System.Windows.Threading;

namespace Sipol.Net.Extensions.Wpf
{
    public static class UIElementExt
    {
        private static Action EmptyDelegate = delegate () { };

        public static void Refresh( this UIElement uiElement )
        {
            uiElement.Dispatcher.Invoke(DispatcherPriority.Render, EmptyDelegate);
        }

    }
}
