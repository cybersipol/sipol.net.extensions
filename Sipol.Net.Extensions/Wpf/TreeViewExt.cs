﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace Sipol.Net.Extensions.Wpf
{
    public static class TreeViewExt
    {
        public static void IterateThroughAllItems(this TreeView tv, Action<object> itemVisitator)
        {
            if (!tv.HasItems)
                return;

            doIterateItems(tv, itemVisitator);
        }

        private static void doIterateItems(object item, Action<object> itemVisitator )
        {
            if (!(item is ItemsControl)) return;

            ItemsControl Item = item as ItemsControl;
            if (!Item.HasItems)
                return;

            foreach (ItemsControl child in Item.Items )
            {
                itemVisitator?.Invoke(child);
                doIterateItems(child, itemVisitator);
            }
        }


        public static TreeViewItem GetParentOfItem( TreeViewItem item )
        {
            DependencyObject parent = item.Parent;

            do
            {
                if (parent is null)
                    return null;

                if (parent is TreeViewItem)
                    return parent as TreeViewItem;

                if (parent is ItemsControl)
                    parent = (parent as ItemsControl).Parent;
                else
                    parent = null;
            }
            while (parent != null && parent is ItemsControl);


            if (parent!=null && parent is TreeViewItem)
                return parent as TreeViewItem;

            return null;
        }

        public static TreeViewItem[] GetParentsOfItem(TreeViewItem item)
        {
            List<TreeViewItem> result = new List<TreeViewItem>();

            TreeViewItem parent = GetParentOfItem(item);

            while (parent!=null)
            {
                result.Add(parent);
                parent = GetParentOfItem(parent);
            }

            return result.ToArray();
        }
    }
}
