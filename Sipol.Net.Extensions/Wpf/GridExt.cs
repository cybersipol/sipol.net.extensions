﻿using System.Windows;
using System.Windows.Controls;

namespace Sipol.Net.Extensions.Wpf
{
    public static class GridExt
    {
        public static void GetRowColumn( this Grid @this, Point position, out int row, out int column )
        {
            column = -1;
            double total = 0;
            int icol = 0;
            foreach (ColumnDefinition clm in @this.ColumnDefinitions)
            {
                total += clm.ActualWidth;
                if (position.X <= total)
                {
                    column = icol;
                    break;
                }
                icol++;
                
            }
            row = -1;
            total = 0;
            int irow = 0;
            foreach (RowDefinition rowDef in @this.RowDefinitions)
            {
                total += rowDef.ActualHeight;
                if (position.Y < total)
                {
                    row = irow;
                    break;
                }
                irow++;
            }
        }
    }
}
