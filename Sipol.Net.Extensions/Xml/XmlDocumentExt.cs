﻿using System;
using System.Text;
using System.Xml;

namespace Sipol.Net.Extensions.Xml
{
    public static class XmlDocumentExt
    {
        public static void IterateThroughAllNodes(
               this XmlDocument doc,
               Action<XmlNode> elementVisitor )
        {
            if (doc != null && elementVisitor != null)
            {
                foreach (XmlNode node in doc.ChildNodes)
                {
                    doIterateNode(node, elementVisitor);
                }
            }
        }

        private static void doIterateNode(
            XmlNode node,
            Action<XmlNode> elementVisitor )
        {
            elementVisitor(node);

            foreach (XmlNode childNode in node.ChildNodes)
            {
                doIterateNode(childNode, elementVisitor);
            }
        }

        public static string FindXPath( XmlNode node, out int IndexInParent, out string nsPrefix, bool useLocalName = false, bool useIndexer = true )
        {
            IndexInParent = -1;
            nsPrefix = String.Empty;
            bool isWasNsPrefix = false;
            StringBuilder builder = new StringBuilder();
            while (node != null)
            {
                switch (node.NodeType)
                {
                    case XmlNodeType.Attribute:
                        builder.Insert(0, "/@" + node.Name);
                        node = ((XmlAttribute)node).OwnerElement;
                        break;
                    case XmlNodeType.Element:
                        int index = FindElementIndex((XmlElement)node);
                        if (IndexInParent < 0 && index > 0) IndexInParent = index;
                        if (!isWasNsPrefix)
                        {
                            int k = node.Name.IndexOf(":");
                            if (k > 0) nsPrefix = node.Name.Substring(0, k);
                            isWasNsPrefix = true;
                        }
                        builder.Insert(0, "/" + (useLocalName ? node.LocalName : node.Name) + (useIndexer ? "[" + index + "]" : ""));
                        node = node.ParentNode;
                        break;
                    case XmlNodeType.Document:
                        return builder.ToString();
                    default:
                        throw new ArgumentException("Only elements and attributes are supported");
                }
            }
            throw new ArgumentException("Node was not in a document");
        }

        public static string FindXPath( XmlNode node, bool useLocalName = false, bool useIndexer = true )
        {
            int indexInParent = -1;
            string nsPrefix = String.Empty;
            return FindXPath(node, out indexInParent, out nsPrefix, useLocalName, useIndexer);
        }

        public static string FindXPath( XmlNode node )
        {
            int indexInParent = -1;
            string nsPrefix = String.Empty;
            return FindXPath(node, out indexInParent, out nsPrefix, false, true);
        }

        public static int FindElementIndex( XmlElement element )
        {
            XmlNode parentNode = element.ParentNode;
            if (parentNode is XmlDocument)
            {
                return 1;
            }
            XmlElement parent = (XmlElement)parentNode;
            int index = 1;
            foreach (XmlNode candidate in parent.ChildNodes)
            {
                if (candidate is XmlElement && candidate.Name == element.Name)
                {
                    if (candidate == element)
                    {
                        return index;
                    }
                    index++;
                }
            }
            throw new ArgumentException("Couldn't find element within parent");
        }

        public static int GetNodeLevel( this XmlDocument doc, XmlNode node )
        {
            if (node == null) return -1;
            if (node.ParentNode is XmlDocument) return 1;

            int level = 0;
            XmlNode current = node.ParentNode;
            do
            {
                level++;
                current = current.ParentNode;
            }
            while (current != null && current is XmlNode);

            return level;
        }


    }
}
